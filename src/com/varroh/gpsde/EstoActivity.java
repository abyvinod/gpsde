package com.varroh.gpsde;

import android.location.Location;
import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;

public class EstoActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_esto);
		
		Button go=(Button) findViewById(R.id.button1);
		go.setOnClickListener(new OnClickListener() {
			
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				
				Location source=new Location("Source");//to inttialize the source
				source.setLatitude(25.2717);
				source.setLongitude(55.2986);
				Location dest=new Location("Destination"); //to initialize the destination 
				dest.setLatitude(10.0144);
				dest.setLongitude(76.3122);
								
				float distanceBetweenpoints=source.distanceTo(dest);
				
				Toast.makeText(getApplicationContext(),"Distance =>"+distanceBetweenpoints, Toast.LENGTH_LONG).show();
							
			}
		});
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.esto, menu);
		return true;
	}

}
