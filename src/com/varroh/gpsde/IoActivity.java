package com.varroh.gpsde;

import android.os.Bundle;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;

public class IoActivity extends Activity {

	Context context=this;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_io);

		Button bfav=(Button) findViewById(R.id.button1);
		Button bgoogle=(Button) findViewById(R.id.button2);
		Button back=(Button) findViewById(R.id.button3);
		
		bgoogle.setOnClickListener(new OnClickListener(){
		public void onClick(View v) {
				// TODO Auto-generated method stub
//				Intent a=new Intent(getApplicationContext(),CurrentActivity.class);
//				startActivity(a);
				Toast.makeText(context, "Google Map",Toast.LENGTH_LONG).show();
			}
		});
		
		bfav.setOnClickListener(new OnClickListener() {
						
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
                Intent b=new Intent(context,FavActivity.class);
                startActivity(b);
				finish();
			}
		});
		
		back.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent i=new Intent(context,ChooseActivity.class);
    	    	startActivity(i);
				finish();
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.io, menu);
		return true;
	}

}
