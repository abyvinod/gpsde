package com.varroh.gpsde;

import android.os.Bundle;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

public class FavActivity extends Activity {
	
	Context context=this;
	RadioGroup rg;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_fav);
		
		final MyDBAdapter md=new MyDBAdapter(getApplicationContext());
//        md.insertgps("Test", 0.0, 0.0);
		Cursor c=md.getAllgps();    
		  
		RadioButton rtemp;
		rg=(RadioGroup) findViewById(R.id.radioGroup1);
		int i=1;
		if(md.N>0)
		{
			c.moveToNext();					// Throwing away the dest
			while(c.moveToNext())
			{
				rtemp=new RadioButton(context);
				rtemp.setId(i);
				i=i+1;
				rtemp.setText(c.getString(1)+". "+c.getString(2));
				rg.addView(rtemp);
			}
		}		
		Button add=(Button) findViewById(R.id.button1);
		Button use=(Button) findViewById(R.id.button2);
		Button del=(Button) findViewById(R.id.button3);
		Button back=(Button) findViewById(R.id.button4);
		
		add.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent i=new Intent(context,AddgpsActivity.class);
				startActivity(i);
				finish();
			}
		});
		
		use.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Integer selected=getsel();
				Cursor c=md.getgps(selected);
				if(c==null)
				{
					Log.d("Debug_favactivity",selected+" is not valid.");
					Toast.makeText(context, "Not a valid selection", Toast.LENGTH_SHORT).show();
				}
				else
				{
					Log.d("Debug_favactivity",c.getString(2)+" "+c.getString(3)+" "+c.getString(4));
					Log.d("Debug_favactivity",selected.toString());
					md.updategps(0,0,c);
					Toast.makeText(context, "Set the destination as "+c.getString(2), Toast.LENGTH_SHORT).show();
					Intent i=new Intent(context,ChooseActivity.class);
	    	    	startActivity(i);
					finish();
				}
			}
		});
		
		del.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Integer selected=getsel();
				md.deleteEntry(selected);
				Toast.makeText(context, "Deleted the entry", Toast.LENGTH_SHORT).show();
				Intent i=new Intent(context,ChooseActivity.class);
    	    	startActivity(i);
				finish();
			}
		});
		
		back.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent i=new Intent(context,IoActivity.class);
    	    	startActivity(i);
				finish();
			}
		});
	}
	
	public Integer getsel()
	{
		Integer selected=rg.getCheckedRadioButtonId();
//		RadioButton b = (RadioButton) findViewById(selected);
//		Toast.makeText(context,b.getText(),Toast.LENGTH_LONG).show();
		return(selected); //Ignore the preference. Hence -1 reqd if you want to see 0 too.
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.fav, menu);
		return true;
	}

}
