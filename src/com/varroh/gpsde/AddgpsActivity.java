package com.varroh.gpsde;

import android.os.Bundle;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class AddgpsActivity extends Activity {

	Context context=this;
	String loc,lat,longt;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_addgps);
		
		final EditText locs=(EditText) findViewById(R.id.editText1);
		final EditText lats=(EditText) findViewById(R.id.editText2);
		final EditText longts=(EditText) findViewById(R.id.editText3);
		
		Button bsave=(Button) findViewById(R.id.button1);
		final AlertDialog.Builder builder = new AlertDialog.Builder(context);
    	final MyDBAdapter md=new MyDBAdapter(context);
    	
		bsave.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Log.d("Debug_addgps","Adding an entry");
				loc=locs.getText().toString();
				lat=lats.getText().toString();
				longt=longts.getText().toString();
				builder.setTitle("Confirm the details");
				builder.setMessage("Is this correct?\n Location :"+loc+"\n Latitude :"+lat+"\n Longitude :"+longt);
		    	builder.setIcon(android.R.drawable.ic_dialog_alert);
		    	builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
		    	    public void onClick(DialogInterface dialog, int which) {			      	
		    	    	//Yes button clicked, do something
		    	    	md.insertgps(loc,lat,longt);
		    	    	Log.d("Debug_admin_review","Inserted the entry");
		    	    	Toast.makeText(context, "Added the entry",Toast.LENGTH_SHORT).show();
		    	    	Intent i=new Intent(context,FavActivity.class);
		    	    	startActivity(i);
						finish();			// No point staying back here. :)
		    	    }
		    	});
		    	builder.setNegativeButton("No", null);
		    	builder.show();		    	
			}
		});
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.addgps, menu);
		return true;
	}

}
