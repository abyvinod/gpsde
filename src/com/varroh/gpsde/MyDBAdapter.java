package com.varroh.gpsde;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public class MyDBAdapter {
	
	// Calls public void onCreate(SQLiteDatabase db)
	MyDBHelper db_helper;
	// Database naming
	String DB_NAME="GPScoord";
	String TAB_NAME="GPSbank";
	// Do most of the communication
	// db_helper is just to do the upper-level 
	SQLiteDatabase db=null;
	String Q_Name,Admin_ID;
	Context cont;
	
	public Integer N=1;			//The total count
	
	public MyDBAdapter(Context context) 
	{
		 cont=context;
		 db_helper = new MyDBHelper(context, DB_NAME, null, 1);
		 Cursor c1=getAllgps();
		 N=c1.getCount()-1;			// N now has the count
		 Log.d("Debug_mydbadapter","Total no. of elements =>"+N);
		 if(N==-1)
		 {
			 insertgps("Equator", "0.0", "0.0");
		 }
		 c1.close();
		 close();					// Closing the link for Qs();
	}
	
	public void open() throws SQLException 
    {
       //open database in reading/writing mode
       db = db_helper.getWritableDatabase();
    } 

	public void close() 
	{
	   if (db != null)
		   db.close();
	}	
	
	public void insertgps(String location, String lat, String longt)
	{
		// ContentValues which is like bundle
		ContentValues bag = new ContentValues();
		// Order matters. It should be as same as the columns
		// Contents of the bag will increase with every put statement
		N=N+1;	
		bag.put("slno", N);
		bag.put("location", location);
		bag.put("lat", lat);
		bag.put("longt", longt);
				
		open();
		//Insert into the table qbank the contents of the bag.
		long row=db.insert(TAB_NAME, null, bag);
		if(row!=-1)
			Log.d("Debug_mydbadapter","Inserted an entry. Function returned row no. => "+row);
		else
			Log.d("Debug_mydbadapter","Error while inserting");
		close();							// Update N
	}
	
	public void updategps(Integer Slnosour, Integer Slnodest,Cursor c)
	{
		// ContentValues which is like bundle
		ContentValues bag = new ContentValues();
		// Order matters. It should be as same as the columns
		// Contents of the bag will increase with every put statement
		bag.put("slno", Slnodest);
		bag.put("location", c.getString(2));
		bag.put("lat", c.getString(3));
		bag.put("longt",c.getString(4));
		
		open();
		//Insert into the table qbank the contents of the bag.
		long row=db.update(TAB_NAME,bag,"Slno=?",new String []{Slnosour.toString()});
		if(row==1)
			Log.d("Debug_mydbadapter","Updated "+Slnosour+" to "+Slnodest);
		else
			Log.d("Debug_mydbadapter","Error as rows updated are "+row+" in no.");
		close();		
	}
	
	public void deleteEntry(Integer Slno)
	{
		open();
		//Insert into the table fruits the contents of the bag.
		Log.d("Debug_mydbadapter","Deleting an entry");
		db.delete(TAB_NAME,"Slno = ?", new String[]{Slno.toString()});
		preparedel(Slno);
		Log.d("Debug_mydbadapter",Slno.toString());
		N=N-1;
		close();
	}
	
	void preparedel(Integer Slno)
	{
		open();
		Cursor c=getAllgps();
		Integer qtemp=0;
		
		while(c.moveToNext())
		{
			qtemp=c.getInt(1);
			if(qtemp>Slno)
			{
				updategps(qtemp,qtemp-1,c);
			}
		}
		c.close();
	}
	
	public void deleteall()
	{
		open();
		String query="DELETE FROM ";
		query=query.concat(TAB_NAME);
		db.execSQL(query);
		Log.d("Debug_settings","Dropped the question paper");

		db_helper = new MyDBHelper(cont, DB_NAME, null, 1);
		
		close();
	}
	
	public Cursor getAllgps()
	{
		open();
		Log.d("Debug_mydbadapter","Asked to fetch the entire database");
		String query="SELECT * FROM ";
		query=query.concat(TAB_NAME);
		Cursor c1 = db.rawQuery(query, null);
		Log.d("Debug_mydbadapter","Fetched the entire database");
		return c1;
	}
	
	public Cursor getgps(Integer Slno)
	{
		open();
		Log.d("Debug_scoredbadapter","Got to fetch "+Slno.toString());
		String query="SELECT * FROM "+TAB_NAME+" WHERE slno=?";
		Cursor c=null;
		if ( (Slno <= N) && (Slno > 0) )
		{
			c=db.rawQuery(query, new String[]{Slno.toString()});
			c.moveToNext();				// Moving the cursor forward
			Log.d("Debug_mydbadapter","Got it");
		}
		else
		{
				Log.d("Debug_mydbadapter","Bad luck");						
		}
		close();
		return c;						// Returns null if failed
	}
}
