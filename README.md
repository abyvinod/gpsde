GPSDE
=====

After tasting success in my first Android venture (https://bitbucket.org/abyvinod/quiz_app), I decided to do another Android 
application to continue pursuing Android Development as a hobby. This project was being jointly developed by my brother, Paul, and me.

This application tends to address a problem faced by a long distance traveller. Assume you are travelling from Chennai to
Ernakulam via bus and you want to know how far off are you from Ernakulam. Google Maps comes to rescue (atleast most of
the times). While travelling (especially in India), you may pass through places where you don't have any network 
coverage. Google Maps won't be able to help you in such situations. Also, if you are among those Indians who value their
money too much to spend on network packs, you will always have second thoughts on using Google Maps. Our application aims
to help you ease your pain in both these scenarios. All smartphones are equipped with GPS modules and the information 
provided by these modules are enough to determine your position. So, if you have your destination typed into this application
you can know how far off you are and if you know your speed of travel, voila, you have the estimate. All this without relying on the (expensive) data plans.

We dropped this project after Google released offline version of Google maps.